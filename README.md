A plugin for showing videos of talks from Debconf, the yearly conference
of the Debian GNU/Linux distribution, as well as some other Debian-related
video talks.

Information is taken from the Debconf Video Team's archive metadata:
https://salsa.debian.org/debconf-video-team/archive-meta

Currently there are two sections:
* Debconf talks, using the metadata
* all video.debian.net videos, by name only

The latter include also a number of other talks, e.g. mini-debconfs, but
lack any other information besides the file name.


Installing
----------

Eventually plugin should be available in standard Kodi repositories. Until
then, you need to pack it into a zip file (or use an existing zip).

A zip with the plugin could be found at https://kodi.tzafrir.org.il/ .
Alternatively you can create it on your own (see instructions below).

You then need to:
1. System -> System Settings -> Addons -> Untrusted Sources (enable)
   and note that this allows installing addons insecurly from the
   Internet.
2. System -> File Manager -> Add source
   Add either https://kodi.tzafrir.org.il/ or a local directory where you
   placed the addon zip file in.
3. Addons -> Install From Repository -> .. -> Install from zip file ->
   [name of the source you added in previous step] ->
   plugin.video.debconf-[version].zip


Building Plugin
---------------

From git:

  git archive --prefix=plugin.video.debconf/ --format=zip >../plugin.video.debconf.zip
